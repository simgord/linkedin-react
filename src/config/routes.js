import React from 'react'

import Login from '../screens/login'
import Characters from '../screens/characters'
import Profile from '../screens/profile'

import {
  Route,
  BrowserRouter as Router,
  Switch,
  Redirect
} from 'react-router-dom'

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route exact path='/' component={Login}></Route>
        <Route path='/characters' component={Characters}></Route>
        <Route path='/profile' component={Profile}></Route>
        <Redirect to='/' />
      </Switch>
    </Router>
  )
}

export default Routes
