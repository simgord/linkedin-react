import React from 'react'
// import ReactPaginate from 'react-paginate'

import axios from 'axios'

const getCharacters = () => {
  axios
    .get(
      `https://gateway.marvel.com:443/v1/public/characters?apikey=7d07b93b15e3a6d8b30def2d0f0601df&ts=1&hash=72f2642423c297fd4552c670add297a9`
    )
    .then(res => {
      console.log(res.data.data.results)
      return res.data.data.results
    })
    .catch(function(error) {
      console.log(error)
    })
}

const Characters = () => {
  let characters = getCharacters()
  return <p>characters</p>
}

export default Characters
