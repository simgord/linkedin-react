import React, { useEffect, useState } from 'react'
// import ReactPaginate from 'react-paginate'
import Post from '../components/posts'
import PostForm from '../components/posts/form'
import styled from 'styled-components'
import Loader from '../components/loader'
import jsonData from '../utils/fakeapi.json'
import iconeProfile from '../icon-profile.png'

import { motion } from 'framer-motion'
import axios from 'axios'

const Profile = props => {
  const [actus, setActus] = useState([])
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    const handler = setTimeout(() => {
      getActu()
    }, 500)
    return () => {
      clearTimeout(handler)
    }
  }, [])

  const getActu = async () => {
    try {
      // console.log(data)
      setActus(jsonData)
      setLoading(false)
      localStorage.setItem('db', JSON.stringify(jsonData))

      // console.log(localStorage)
    } catch (err) {
      console.log(err)
    }
  }
  return (
    <div>
      <Header>
        <DivImage>
          <IconeProfile src={iconeProfile}></IconeProfile>
        </DivImage>
        <DivSearch>
          <InputSearch placeholder='Chercher un utilisateur (1, 2, ...)'></InputSearch>
        </DivSearch>
      </Header>
      <PostForm setActus={setActus}></PostForm>
      {loading ? (
        <Loader />
      ) : (
        <>
          {actus.length > 0 ? (
            <div className='actus'>
              {actus.map(post => (
                <Post key={post.id} post={post} history={props.history}></Post>
              ))}
            </div>
          ) : (
            <></>
          )}
        </>
      )}
    </div>
  )
}
const IconeProfile = styled.img`
  width: 100%;
  height: 100%;
`
const DivImage = styled.div`
  width: 64px;
  height: 64px;
`
const DivSearch = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
`

const Header = styled.div`
  background-color: blue;
  display: flex;
  padding: 12px;
`

const InputSearch = styled.input`
  box-sizing: border-box;
  height: 45px;
  padding: 0px 10px;
  outline: none;
  border: none;
  margin: 1px 0px 0px 5px;
  border: 1px solid rgba(0, 0, 0, 0.6);
  width: 80%;
`

export default Profile
