import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import LikeIcon from '../../png/like.png'
import DislikeIcon from '../../png/dislike.png'
import CommentIcon from '../../png/comment.png'
import ShareIcon from '../../png/share.png'
import styled from 'styled-components'
import iconeProfile from '../../icon-profile.png'

const Posts = ({ post }) => {
  const [counter, setCounter] = useState(0)
  useEffect(() => {}, [counter])

  const [like, setLike] = useState(false)

  const likePost = () => {
    setCounter(counter + 1)
    setLike(true)
  }

  const dislikePost = () => {
    setCounter(counter - 1)
    setLike(false)
  }

  return (
    <AllContainer>
      <DivPublication>
        <DivImage>
          <IconeProfile src={iconeProfile}></IconeProfile>
        </DivImage>
        <DivID>
          <h1>Utilisateur : {post.userId}</h1>
        </DivID>
      </DivPublication>

      <DivDetails>
        <ParagrapheDetail>{post.body}</ParagrapheDetail>
      </DivDetails>

      <DivPublication>
        {like ? (
          <DivLike onClick={() => dislikePost()}>
            <DivIcon>
              <IconeProfile src={LikeIcon}></IconeProfile>
            </DivIcon>
            <DivTools>Je n'aime plus</DivTools>
          </DivLike>
        ) : (
          <DivLike onClick={() => likePost()}>
            <DivIcon>
              <IconeProfile src={DislikeIcon}></IconeProfile>
            </DivIcon>
            <DivTools>J'aime</DivTools>
          </DivLike>
        )}
        <DivComment>
          <DivIcon>
            <IconeProfile src={CommentIcon}></IconeProfile>
          </DivIcon>
          <DivTools>Commentez</DivTools>
        </DivComment>
        <DivShare>
          <DivIcon>
            <IconeProfile src={ShareIcon}></IconeProfile>
          </DivIcon>
          <DivTools>Partagez</DivTools>
        </DivShare>
      </DivPublication>
    </AllContainer>
  )
}
Posts.propTypes = {
  post: PropTypes.object
}

const IconeProfile = styled.img`
  width: 100%;
  height: 100%;
`
const DivLike = styled.div`
  display: flex;
  align-items: left;
  justify-content: center;
  width: 100%;
`

const DivShare = styled.div`
  display: flex;
  align-items: right;
  justify-content: center;
  width: 100%;
`

const DivComment = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
`

const AllContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  padding: 12px;
  border: none;
  border-bottom: 1px solid black;
  border-top: 1px solid black;
`
const DivImage = styled.div`
  width: 64px;
  height: 64px;
`

const DivIcon = styled.div`
  width: 24px;
  height: 24px;
`

const DivID = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
`
const DivTools = styled.div`
  display: flex;
  margin-left: 5px;
  width: 100%;
`
const DivPublication = styled.div`
  display: flex;
  border: none;
  margin-bottom: 5px;
`

const DivDetails = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`

const ParagrapheDetail = styled.p`
  text-align: center;
  padding: 12px;
`
export default Posts
