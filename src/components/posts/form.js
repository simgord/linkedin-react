import React, { useState } from 'react'
import jsonData from '../../utils/fakeapi.json'

import styled from 'styled-components'

export default function PostForm({ setActus }) {
  const [postBody, setPostBody] = useState('')

  const handlePostBodyChange = event => {
    setPostBody(event.target.value)
  }

  const handleSubmit = async event => {
    let newDB = JSON.parse(localStorage.getItem('db'))
    console.log(typeof newDB)
    event.preventDefault()
    newDB.unshift({
      userId: 'Hikkary',
      id: 101,
      title: 'Publication de test',
      body: postBody
    })
    console.log(newDB)
    JSON.stringify(newDB)
    localStorage.setItem('db', JSON.stringify(newDB))
    setActus(newDB)
    setPostBody('')
  }

  return (
    <div>
      <form className='post-form' onSubmit={handleSubmit}>
        <InputContainer>
          <StyledInput
            type='text'
            value={postBody}
            onChange={handlePostBodyChange}
            placeholder='Partager quelquechose...'
            required
          ></StyledInput>

          <StyledButton onClick={handleSubmit}>Publier</StyledButton>
        </InputContainer>
      </form>
    </div>
  )
}

const InputContainer = styled.div`
  padding: 12px;
  display: flex;
  flex-direction: column;
`
const StyledInput = styled.input`
  box-sizing: border-box;
  height: 45px;
  padding: 0px 10px;
  outline: none;
  border: none;
  margin: 1px 0px 0px 5px;
  border: 1px solid rgba(0, 0, 0, 0.6);
`

const StyledButton = styled.button`
  background-color: #0073b1;
  height: 45px;
  padding: 0px 10px;
  outline: none;
  border: none;
  margin: 1px 0px;
`
